﻿using UnityEngine;
using System.Collections;

public class TestMonsterController : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void OnCollisionEnter(Collision col){
		
		if(col.transform.tag == "Player")
		{
			print("Monster attacked");
			PlayerStatus PS = col.transform.GetComponent<PlayerStatus>();
			PS.damageEntity(25);
			print(PS.getGenericStatus(0).getCurrent());

		}
	}
}
