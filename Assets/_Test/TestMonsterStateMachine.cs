using UnityEngine;
using Waypoint;
public class TestMonsterStateMachine:MonoBehaviour{

	//move between nodes
	
	//move towards player when seen
	
	//move towards player last posision when player is out of sight 
	//will do the above action for a long timer amount of time.
	
	// will wait at players last known position for a short period of time
	
	//if player not respotted, return to his own last position.
	
	NavMeshAgent nav;

	public WayPointByVector WayPointList;
	public Vector3 lastPersonalPosition;
	public Vector3 lastPlayerPosition;
	public Transform player;
	
	const float WAIT = 3.0f;
	const float RUN  = 30.0f;
	
	float waitTimer = 0f;
	float runTimer = 0f;

	int mode = 0;
	
	public void Start(){
		waitTimer = 10.0f;
		nav = this.transform.GetComponent<NavMeshAgent>();
	
		for (int i = 0; i < 5; i++)
			Debug.LogWarning ("Start test: " + this.WayPointList.GetTarget (i));
	}
	
	public void FixedUpdate(){
		
		if(mode == 0){//player not spoted 
			waitTimer += Time.deltaTime;//increase wait.
			
			if(waitTimer >= WAIT){
				waitTimer = 0;

				this.nav.destination = this.WayPointList.GetTarget (Random.Range(0,WayPointList._targets.Length-1));
				Debug.LogWarning("New Nav: "+this.nav.destination);
				///needs code!
				//.. do action to move to a random node;
				
				
			}//end inner if
		}//end if
		
		else if (mode == 1){//player spotted 
			//if (lastPlayerPosition != player.position){
			//	print("position different");
			//	newTargetPosition(player);
			//}
		}
		else if (mode == 2){//lose sight of player 
			runTimer+= Time.deltaTime;
			if(runTimer>= RUN){
				//returns monster to default action
				player = null; //just clean up to make sure the monster doesn't randomly chase the player.
				SwitchMode(0);
				waitTimer = WAIT; //set the time to it's max duration so the monster picks a new node
			}
		}
		else if(mode == 3){//waiting at players last known location;
			waitTimer += Time.deltaTime;
			if (CheckTimer(waitTimer,WAIT)){
				SwitchMode(0);
				waitTimer = WAIT; //set the time to it's max duration so the monster picks a new node
			}
		}
		
	}
	

	public void OnCollisionEnter(Collision col){
		

	}
	public void OnCollisionStay(Collision col){}
	public void OnCollisionExit(Collision col){}

	public void OnTriggerEnter(Collider col){
		if((mode == 0 || mode == 2 || mode == 3) && col.transform.tag == "Player" ){
			SwitchMode(1);	
			newTargetPosition(col.transform);

		}
	}
	public void OnTriggerStay(Collider col){}
	public void OnTriggerExit(Collider col){}

	public static bool CheckTimer(float timer, float conditional){
		return timer >= conditional;//returns true if timer is greater than conditional.
	}
	
	public void SwitchMode(int newMode){
		this.mode = newMode;
		
		//resets timers to default value.
		waitTimer = 0;
		runTimer = 0;
	}
	
	public void newTargetPosition(Transform t){
		this.player = t;
		this.newTargetPosition();
	}
	public void newTargetPosition(){
		nav.destination = transform.position;
	}
}
