﻿using UnityEngine;
using System.Collections;

public class CyberPickup : Pickup {
	
	
	public int index = 0;
	public int value = 0;
	// Use this for initialization
	
	public override void applyPickup(PlayerStatus ps){
		ps.GetComponent<PlayerStatus>().getGenericStatus(index).appendCurrent(value);
	}
}
