﻿using UnityEngine;
using System.Collections;

public class CyberHealthPickup : Pickup {
	public override void applyPickup(PlayerStatus player){
		if(player.getHealth().getCurrent() <100){
			player.getHealth().appendCurrent(25);
			this.gameObject.SetActive(false);
		}
	}


}
