﻿using UnityEngine;
using System.Collections;

public class Point : MonoBehaviour {

	public int spin_X = 1;
	public int spin_Y = 1;
	public int spin_Z = 1;
	
	// Update is called once per frame
	public virtual void FixedUpdate () {
		this.transform.Rotate(new Vector3(spin_X,spin_Y,spin_Z));	
	}


	protected virtual void InteralAction(){

	}
	protected virtual void DestroyAction(float delayTime){
		GameObject.Destroy(this,delayTime);
		GameObject.Destroy (this.transform.parent,delayTime);
	}


	public virtual void Action(){
		InteralAction ();
		//DestroyAction (0);

	}

}
