﻿using UnityEngine;
using System.Collections;

public class PlayerCollision : MonoBehaviour {
	
	void OnControllerColliderHit(ControllerColliderHit hit){
		
	}
	
	
	void OnCollisionExit(Collision collision){
	
	}
	
	void OnTriggerEnter(Collider collision){
		if(collision.transform.tag.Equals("PickUp")){
			collision.transform.GetComponent<Pickup>().applyPickup(this.transform.GetComponent<PlayerStatus>());
		}
	}
	void OnTriggerExit(Collider collision){
	
	}
	void OnTriggerStay(Collider collision){

	}
}
