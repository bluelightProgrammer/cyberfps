﻿using UnityEngine;
using System.Collections;
using Waypoint;

namespace Waypoint{
	public class WayPointByChildTransform : WayPointByVector {

		// Use this for initialization
		void Awake () {
			Transform[] T = this.transform.GetComponentsInChildren<Transform> ();
			Vector3[] V = base._targets;
			base._targets = new Vector3[V.Length+T.Length];

			for (int i = 0; i < V.Length; i++) {
				base._targets[i] = V[i];
			}

			for (int i = V.Length; i < V.Length+T.Length; i++) {
				base._targets[i] = T[i].position;
			}
		}
	
	
	}
}