﻿using System;
using System.Collections;

public class DynamicNumber {
	private int minimum = 0;
	private int maximum = 100; 
	private int value = 0;

	public DynamicNumber(int min, int max, int current){
		max = Math.Max(max,max);
		min = Math.Min(min,max);
		
		this.setCurrent(current);
	}
	
	public void setBound(int min, int max){
		minimum = Math.Min(min,max);
		maximum = Math.Max(max,min);
		
		this.setCurrent(this.value);
	}
	
	public void appendCurrent(int increaseValue){
		this.setCurrent(this.value + increaseValue);
	}

	public void setCurrent(int newCurrent){
		this.value = Math.Max(minimum,Math.Min(maximum,newCurrent));
	}
	public int getMin(){
		return this.minimum;	
	}
	public int getMax(){
		return this.maximum;	
	}
	public int getCurrent(){
		return this.value;
	}
}
