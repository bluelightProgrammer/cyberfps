﻿using UnityEngine;
using System.Collections;

public class Pickup : Point {
	
	float EmergencyTimer = 0;
	
	public float floorDistance;
	public float hoverBobbingSpeed = 1.0f;
	public float hoverDistance = 0;
	bool isBobbingUp = false;
	Rigidbody RB;
	// Use this for initialization
	void Start () {



		this.tag = "PickUp";


		Collider bc = this.transform.GetComponent<BoxCollider> ();


		RB = bc.attachedRigidbody;
		//RB = this.transform.GetComponent<Rigidbody>();
		


		//RigidbodyConstraints RBC// = this.RB.constraints;///base.spin_X == 0;

		foreach (CharacterController go in GameObject.FindObjectsOfType<CharacterController>())
			Physics.IgnoreCollision (bc, go.GetComponent<Collider>());

		//RBC.
	}
	
	
	public void OnCollisionEnter(Collision col){
		EmergencyTimer =0;
		if(col.transform.tag != "Player"){
			this.gameObject.GetComponent<Rigidbody>().AddForce(20*Vector3.up*Time.deltaTime,ForceMode.VelocityChange);
		}
	}
//	public void OnCollisionStay(Collision col){
//		this.gameObject.GetComponent<Rigidbody>().AddForce(20*Vector3.up*Time.deltaTime,ForceMode.VelocityChange);
//	}
	
	public override void FixedUpdate(){
		base.FixedUpdate();
		
		
		
		//float thisY = this.transform.position.y;
	
		//if(thisY - this.floorDistance <= 0){
			//this.gameObject.GetComponent<Rigidbody>().AddForce(,ForceMode);
		//	this.gameObject.GetComponent<Rigidbody>().AddForce(20*Vector3.up*Time.deltaTime,ForceMode.VelocityChange);
		//}
	/*
	
		//if the distance traveled  from source location is greater than hover distance
		if(Mathf.Abs(thisY -floorDistance) > hoverDistance)
			isBobbingUp = !isBobbingUp;//then start moving in the opposite dirrecction
			
/		Vector3 currentPosition = this.transform.position; // Gameobjects current position.
		RB.MovePosition(Vector3.MoveTowards(currentPosition, currentPosition + (isBobbingUp?Vector3.up:Vector3.down) , hoverBobbingSpeed* Time.deltaTime));//moves the current position closer to it's goal.
		//
		//currentPosition*/
	}

	public virtual void applyPickup(PlayerStatus player){}
}
