﻿using UnityEngine;
using System.Collections;

public class PlayerStatus : MonoBehaviour {
	const int HEALTH = 0;
	const int ARMOR = 1;
	
	private DynamicNumber[] status = new DynamicNumber[]{
		new DynamicNumber(0,100,100), //health
		new DynamicNumber(0,100,100), //armor
		new DynamicNumber(0,100,100)
	}; 
	
	const int MELEE = 0;
	const int PISTOL = 1;
	private bool[] hasWeapon = new bool[]{ true,true,false};
	
	
	
	public void damageEntity(int damage){
		//Easy access to the players health and armor.
		DynamicNumber armor = getGenericStatus(PlayerStatus.ARMOR);
		DynamicNumber health = getGenericStatus(PlayerStatus.HEALTH);
		
		

		
		health.appendCurrent(-damage);
		armor.appendCurrent(-damage);
	}
	
	public DynamicNumber getGenericStatus(int index){
		if(index>=0 && index<status.Length)
			return status[index];
		return null;
	
	}
	
	public DynamicNumber getHealth(){
		return getGenericStatus(PlayerStatus.HEALTH);
	}
	public DynamicNumber getArmor(){
		return getGenericStatus(PlayerStatus.ARMOR);
	}
	
}
