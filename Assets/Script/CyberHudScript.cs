﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CyberHudScript : MonoBehaviour {
	public Text health_T;
	public Text armor_T;
	
	public void Update(){
		health_T.text = "Health: "+GameObject.FindObjectOfType<PlayerStatus>().getHealth().getCurrent();
		armor_T.text = "Armor: "+GameObject.FindObjectOfType<PlayerStatus>().getArmor().getCurrent();
	}
	
}
