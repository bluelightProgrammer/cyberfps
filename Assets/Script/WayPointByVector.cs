﻿using UnityEngine;
using System.Collections;

namespace Waypoint{
	public class WayPointByVector : MonoBehaviour {
		public Vector3[] _targets;

		public virtual Vector3 GetTarget(int index){
			if (_targets.Length == 0)
				return Vector3.zero;

			if (index < 0)
				index = _targets.Length + index;
			else
				index = index % _targets.Length;
			return _targets [index];

		}
	}
}